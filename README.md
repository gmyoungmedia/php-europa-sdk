# TU Europa SDK for PHP
---
This repository contains the PHP SDK that allows you to access the [TU Europa](https://tueuropa.pl/) API from your project.

## Installation
---
The TU Europa PHP SDK can be installed with [Composer](https://getcomposer.org/). Run this command:
```
composer require youngmedia-pl/europa-sdk
```

## Usage
---
Simple example of getting quotes.

```php
<?php

require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$europa = new Europa\Europa([
    'customer_id' => '{customer-id}'
]);

// Use one of our helper classes, currently only one is provided.
// $helper = $europa->getSportCompetitionInsuranceHelper();
$helper = $europa->getSportCompetitionInsuranceHelper();

try {
    // Set params
    $helper->paramsToSet()
        ->sumInsured('30000')
        ->participationFee('60.99')
        ->competitionDate('2020-12-31T08:00:00+02:00')
        ->participantBirthDate('1950-12-31');

    // Get quotes
    $quotes = $helper->getQuotes();
} catch (\InvalidArgumentException $e) {
    // When params validation fails
    echo 'Params validation failed: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error
    echo 'API returned an error: ' . json_decode($e->getMessage());
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues
    echo 'Europa SDK returned an error: ' . json_decode($e->getMessage());
    exit;
}

// Print details
var_dump($quotes);

```
