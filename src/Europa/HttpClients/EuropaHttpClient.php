<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\HttpClients;

/**
 * Interface EuropaHttpClient
 *
 * @package Europa
 */
interface EuropaHttpClient
{
    /**
     * Sends a request to the server and returns the response.
     *
     * @param string $url     The endpoint to send the request to.
     * @param string $method  The request method.
     * @param array  $headers The request headers.
     * @param string $body    The body of the request.
     * @param int    $timeOut The timeout in seconds for the request.
     *
     * @return ApiResponse Response from the server.
     *
     * @throws \Europa\Exceptions\EuropaSDKException
     */
    public function send($url, $method, array $headers, $body, $timeOut);
}
