<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\HttpClients;

use Europa\Http\ApiCurlResponse;
use Europa\Exceptions\EuropaSDKException;

/**
 * Class EuropaCurlHttpClient
 *
 * @package Europa
 */
class EuropaCurlHttpClient implements EuropaHttpClient
{
    /**
     * @var string|boolean The raw response from the server
     */
    protected $rawResponse;

    /**
     * @var EuropaCurl Procedural curl as object
     */
    protected $europaCurl;

    /**
     * Instantiates a new EuropaCurlHttpClient object.
     */
    public function __construct()
    {
        $this->europaCurl = new EuropaCurl();
    }

    /**
     * @inheritdoc
     */
    public function send($url, $method, array $headers, $body, $timeOut)
    {
        $this->openConnection($url, $method, $headers, $body, $timeOut);
        $this->executeConnection();

        if ($errno = $this->europaCurl->errno()) {
            throw new EuropaSDKException($this->europaCurl->error(), $errno);
        }

        $this->closeConnection();

        $apiResponse = new ApiCurlResponse($this->rawResponse);

        return $apiResponse;
    }

    /**
     * Opens a new curl connection.
     *
     * @param string $url     The endpoint to send the request to.
     * @param string $method  The request method.
     * @param array  $headers The request headers.
     * @param string $body    The body of the request.
     * @param int    $timeOut The timeout in seconds for the request.
     */
    public function openConnection($url, $method, array $headers, $body, $timeOut)
    {
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => $this->compileRequestHeaders($headers),
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => $timeOut,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            // CURLOPT_CAINFO => __DIR__ . '/certs/DigiCertGlobalRootG2.pem',
            CURLOPT_CAINFO => __DIR__ . '/certs/ds_cacert.pem',
            CURLOPT_RETURNTRANSFER => true,
            CURLINFO_HEADER_OUT => true, // debug
        ];

        if ($method !== "GET") {
            $options[CURLOPT_POSTFIELDS] = $body;
        }

        $this->europaCurl->init();
        $this->europaCurl->setoptArray($options);
    }

    /**
     * Send the request and get the raw response from curl
     */
    public function executeConnection()
    {
        $this->rawResponse = $this->europaCurl->exec();
    }

    /**
     * Closes an existing curl connection
     */
    public function closeConnection()
    {
        $this->europaCurl->close();
    }

    /**
     * Compiles the request headers into a curl-friendly format.
     *
     * @param array $headers The request headers.
     *
     * @return array
     */
    public function compileRequestHeaders(array $headers)
    {
        $rawHeaders = [];

        foreach ($headers as $key => $value) {
            $rawHeaders[] = $key . ': ' . $value;
        }

        return $rawHeaders;
    }
}
