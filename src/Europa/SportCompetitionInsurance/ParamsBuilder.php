<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

/**
 * Class ParamsBuilder
 *
 * @package Europa
 */
abstract class ParamsBuilder
{
    /**
     * @var array The defined parameters.
     */
    protected $params;

    /**
     * Instantiates a new ParamsBuilder builder.
     *
     * @param array $params the defined parameters.
     */
    public function __construct(array $params)
    {
        $this->setImmutableParams($params);
    }

    /**
     * Sets immutable parameters.
     *
     * @param array $params
     */
    public function setImmutableParams($params)
    {
        $this->params = [];

        foreach ($params as $param => $value) {
            if ($value instanceof \DateTime) {
                $value = \DateTimeImmutable::createFromMutable($value);
            }
            $this->params[$param] = $value;
        }
    }

    /**
     * Builds and returns request parameters.
     *
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    public function validateAndBuild()
    {
        $this->validate();

        return $this->safelyBuild();
    }

    /**
     * Cross validates parameters.
     *
     * @throws \InvalidArgumentException
     */
    abstract public function validate();

    /**
     * Safely builds and returns request parameters.
     *
     * @return array
     */
    abstract public function safelyBuild();
}
