<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

/**
 * Class PolicyParamsBuilder
 *
 * @package Europa
 */
class PolicyParamsBuilder extends ParamsBuilder
{
    /**
     * @const array Required parameters, all others are optional.
     */
    const REQUIRED_PARAMS = [
        'calculation_id',
        'calculation_checksum',
        'payment_date',
        'solicitor_id',
        'agent_id',
    ];

    /**
     * @inheritdoc
     */
    public function validate()
    {
        // Required parameters
        foreach (static::REQUIRED_PARAMS as $param) {
            if (!array_key_exists($param, $this->params)) {
                throw new \InvalidArgumentException('Required param "' . $param . '" is missing.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safelyBuild()
    {
        $dt = new \DateTimeImmutable();

        return [
            'calculation_id' => $this->params['calculation_id'],
            'checksum' => $this->params['calculation_checksum'],
            'payment_date' => $this->params['payment_date']->format('c'),
            'policy_date' => $dt->format('c'),
            'solicitors' => [
                [
                    'solicitor_id' => $this->params['solicitor_id'],
                    'agent_id' => $this->params['agent_id'],
                    'permissions' => ['ZAW'],
                ],
            ],
        ];
    }
}
