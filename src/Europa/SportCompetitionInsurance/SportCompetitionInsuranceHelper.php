<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

use Europa\EuropaOperator;
use Europa\EuropaClient;
use Europa\EuropaRequest;
use Europa\ApiObjects\ApiList;
use Europa\HttpClients\EuropaCurlHttpClient;

/**
 * Class SportCompetitionInsuranceHelper
 *
 * @package Europa
 */
class SportCompetitionInsuranceHelper
{
    /**
     * @var EuropaOperator The EuropaOperator entity.
     */
    protected $operator;

    /**
     * @var EuropaClient The Europa client service.
     */
    protected $client;

    /**
     * @var string The API version we want to use.
     */
    protected $apiVersion;

    /**
     * @var ParamsCollector The request parameters collector.
     */
    protected $paramsCollector;

    /**
     * Initialize the helper.
     *
     * @param EuropaOperator        $operator           The EuropaOperator entity.
     * @param EuropaClient          $client             The client to make HTTP requests.
     * @param string                $apiVersion         The API version we want to use.
     */
    public function __construct(EuropaOperator $operator, EuropaClient $client, $apiVersion)
    {
        $this->operator = $operator;
        $this->client = $client;
        $this->apiVersion = $apiVersion;
        $this->paramsCollector = new ParamsCollector();
    }

    /**
     * Returns sanitized registration form quotes.
     *
     * @return array
     */
    public function getQuotes()
    {
        $formQuotes = $this->getUnsanitizedQuotes();

        return $this->sanitizeQuotes($formQuotes);
    }

    /**
     * Sanitize quotes before passing to frontend.
     *
     * @param array $quotes Array of ApiQuote's to be sanitized.
     *
     * @return array
     */
    public function sanitizeQuotes($quotes)
    {
        $sanitizedQuotes = [];

        foreach ($quotes as $name => $quote) {
            if ($quote === null) {
                continue;
            }
            $sanitizedQuotes[$name] = [
                'product_id' => $quote->getProductId(),
                'name' => $quote->getName(),
                'value' => $quote->getPremium()->getValue(),
                'currency' => $quote->getPremium()->getValueCurrency(),
            ];
        }

        return $sanitizedQuotes;
    }

    /**
     * Returns unsanitized registration form quotes.
     *
     * @return array
     */
    public function getUnsanitizedQuotes()
    {
        $formQuotes = [];
        $formQuotes += $this->getAmateurQuotes();
        $formQuotes += $this->getProfessionalQuotes();

        return $formQuotes;
    }

    /**
     * Returns quotes for amateur.
     *
     * @return array
     */
    public function getAmateurQuotes()
    {
        return $this->getBaseAndRiskQuotes(false);
    }

    /**
     * Returns quotes for professional.
     *
     * @return array
     */
    public function getProfessionalQuotes()
    {
        return $this->getBaseAndRiskQuotes(true);
    }

    /**
     * Returns base and risk quotes for amateur or professional.
     *
     * @param boolean $isProfessional True if participant is professional.
     *
     * @return array
     */
    public function getBaseAndRiskQuotes($isProfessional)
    {
        $params = $this->paramsCollector->getParams();
        $params['is_professional'] = $isProfessional;
        $params['is_resignation_risk'] = true;

        $quotes = $this->getAllQuotes($params);

        list($baseQuote, $riskQuote) = $this->filterBaseAndRiskQuotes($quotes);

        if ($isProfessional) {
            return [
                'professional_base' => $baseQuote,
                'professional_risk' => $riskQuote,
            ];
        } else {
            return [
                'amateur_base' => $baseQuote,
                'amateur_risk' => $riskQuote,
            ];
        }
    }

    /**
     * Gets quotes from API.
     *
     * @param array $params Collected parameters.
     *
     * @return array
     *
     * @throws EuropaSDKException
     */
    public function getAllQuotes(array $params)
    {
        $builder = new QuotesParamsBuilder($params);
        $params = $builder->validateAndBuild();

        $request = $this->request('POST', '/quotes', $params);

        $response = $this->client->sendRequest($request);

        return $response->getQuotes();
    }

    /**
     * Filters and returns base and risk (if available) quotes.
     *
     * @param ApiList $quotes ApiList of ApiQuote's.
     *
     * @return array
     */
    public function filterBaseAndRiskQuotes(ApiList $quotes)
    {
        $base_quote = null;
        $risk_quote = null;
        foreach ($quotes as $quote) {
            if ($quote->hasResignationRisk()) {
                // Quote with resignation risk defined
                if ($this->shouldPickOtherQuote($risk_quote, $quote)) {
                    $risk_quote = $quote;
                }
            } else {
                // Quote without resignation risk defined
                if ($this->shouldPickOtherQuote($base_quote, $quote)) {
                    $base_quote = $quote;
                }
            }
        }
        return [$base_quote, $risk_quote];
    }

    /**
     * Determines if other quote should be picked instead of current.
     *
     * @param \Europa\ApiObjects\ApiQuote $quote       Currently picked quote.
     * @param \Europa\ApiObjects\ApiQuote $other_quote Other candidate quote.
     *
     * @return boolean
     */
    public function shouldPickOtherQuote($quote, $other_quote)
    {
        return !$quote || $other_quote->getPremium()->getValue() < $quote->getPremium()->getValue();
    }

    /**
     * Makes calculation via API.
     *
     * @return \Europa\ApiObjects\ApiCalculation
     *
     * @throws InvalidArgumentException
     * @throws EuropaSDKException
     */
    public function makeCalculation()
    {
        $builder = new CalculationParamsBuilder($this->paramsCollector->getParams());
        $params = $builder->validateAndBuild();

        $request = $this->request('POST', '/policies/calculate', $params);

        $response = $this->client->sendRequest($request);

        return $response->getCalculation();
    }

    /**
     * Issues policy via API.
     *
     * @return \Europa\ApiObjects\ApiPolicy
     *
     * @throws InvalidArgumentException
     * @throws EuropaSDKException
     */
    public function issuePolicy()
    {
        $builder = new PolicyParamsBuilder($this->paramsCollector->getParams());
        $params = $builder->validateAndBuild();

        $request = $this->request('POST', '/policies/issue', $params);

        $response = $this->client->sendRequest($request);

        return $response->getPolicy();
    }

    /**
     * Instantiates a new EuropaRequest entity.
     *
     * @param string    $method
     * @param string    $endpoint
     * @param array     $params
     *
     * @return EuropaRequest
     */
    public function request($method, $endpoint, array $params = [])
    {
        return new EuropaRequest(
            $this->operator,
            $method,
            $endpoint,
            $params,
            $this->apiVersion
        );
    }

    /**
     * Returns request parameters collector.
     *
     * @return ParamsCollector
     */
    public function paramsToSet()
    {
        $this->paramsCollector = new ParamsCollector();

        return $this->paramsCollector;
    }
}
