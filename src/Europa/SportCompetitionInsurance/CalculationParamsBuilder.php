<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

/**
 * Class CalculationParamsBuilder
 *
 * @package Europa
 */
class CalculationParamsBuilder extends ParamsBuilder
{
    /**
     * @const array Required parameters, all others are optional.
     */
    const REQUIRED_PARAMS = [
        'product_id',
        'sum_insured',
        'participation_fee',
        'competition_name',
        'competition_date',
        'agreements',
        'is_professional',
        'is_resignation_risk',
        'participant_first_name',
        'participant_last_name',
        'participant_birth_date',
        'participant_pesel',
        'participant_email',
        'participant_phone',
        'participant_country',
        'participant_city',
        'participant_zipcode',
        'participant_street',
        'participant_house_number',
    ];

    /**
     * @const array Required agreements codes.
     */
    const REQUIRED_AGREEMENTS = ['ZGOBJ', 'ZGPOTW', 'ZGREGODS'];

    /**
     * @const array Agreements codes and descriptions.
     */
    const AGREEMENTS_DATA = [
        'ZGOBJ'     => 'Wyrażam zgodę na objęcie mnie ochroną ubezpieczeniową przez Towarzystwo Ubezpieczeń Europa S.A.',
        'ZGPOTW'    => 'Potwierdzam otrzymanie i zapoznanie się z Ogólnymi Warunkami Ubezpieczenia przed zawarciem umowy ubezpieczenia oraz ich zrozumienie i akceptację.',
        'ZGREGODS'  => 'Oświadczam, że zapoznałem(am) się z Regulaminem świadczenia usług drogą elektroniczną i go akceptuję. Oświadczam, że zostałem(am) poinformowany(a) o prawie odstąpienia od umowy ubezpieczenia w ciągu 30 dni od daty jej zawarcia. Z tym, że w przypadku złożenia oświadczenia o zawarciu umowy ubezpieczenia z zastosowaniem środków porozumiewania się na odległość termin ten liczony jest od dnia otrzymania potwierdzenia zawarcia umowy ubezpieczenia lub od dnia potwierdzenia informacji, o którym mowa w art. 39 ust. 3 ustawy o prawach konsumenta.',
        'ZGMARK'    => 'Wyrażam zgodę na przetwarzanie moich danych osobowych w celach promocyjnych i marketingowych przez Grupę Ubezpieczeniową Europa, w skład której wchodzą TU Europa S.A. i TU na Życie Europa S.A. Zostałem(am) poinformowany(a), że przysługuje mi prawo dostępu do treści moich danych osobowych oraz ich poprawiania. Podanie danych jest dobrowolne.',
        'ZGHAND'    => 'Wyrażam zgodę na otrzymywanie od Grupy Ubezpieczeniowej Europa informacji handlowej drogą elektroniczną.',
        'ZGMBEZ'    => 'Wyrażam zgodę na używanie przez Grupę Ubezpieczeniową Europa telekomunikacyjnych urządzeń końcowych i automatycznych systemów wywołujących dla celów marketingu bezpośredniego.',
        'ZGMAIL'    => 'Wyrażam zgodę na otrzymywanie od ubezpieczyciela korespondencji za pośrednictwem adresu e-mail podanego przeze mnie w niniejszym wniosku o zawarcie umowy ubezpieczenia lub w złożonym oświadczeniu o zmianie danych do umowy ubezpieczenia oraz dodatkowo za pośrednictwem serwisu internetowego ubezpieczyciela.',
    ];

    /**
     * @inheritdoc
     */
    public function validate()
    {
        // Required parameters
        foreach (static::REQUIRED_PARAMS as $param) {
            if (!array_key_exists($param, $this->params)) {
                throw new \InvalidArgumentException('Required param "' . $param . '" is missing.');
            }
        }
        // Required agreements
        foreach (static::REQUIRED_AGREEMENTS as $code) {
            if (!in_array($code, $this->params['agreements'])) {
                throw new \InvalidArgumentException('Required agreement "' . $code . '" is missing.');
            }
        }
        // Validate pesel against birthdate
        $peselBirthdate = substr($this->params['participant_pesel'], 0, 6);
        $participantBirthdate = $this->params['participant_birth_date']->format('ymd');
        if ($peselBirthdate !== $participantBirthdate) {
            throw new \InvalidArgumentException('Pesel and participant birthdate does not match.');
        }
    }

    /**
     * @inheritdoc
     */
    public function safelyBuild()
    {
        $data = [
            'type' => 'private',
            'first_name' => $this->params['participant_first_name'],
            'last_name' => $this->params['participant_last_name'],
            'birth_date' => $this->params['participant_birth_date']->format('Y-m-d'),
            'pesel' => $this->params['participant_pesel'],
        ];

        $address = [
            'country' => $this->params['participant_country'],
            'city' => $this->params['participant_city'],
            'post_code' => $this->params['participant_zipcode'],
            'street' => $this->params['participant_street'],
            'house_no' => $this->params['participant_house_number'],
        ];
        if (isset($this->params['participant_flat_number'])) {
            $address['flat_no'] = $this->params['participant_flat_number'];
        }

        $agreements = [];
        foreach (static::AGREEMENTS_DATA as $code => $description) {
            $agreements[] = [
                'code' => $code,
                'value' => in_array($code, $this->params['agreements']),
                'description' => $description,
            ];
        }

        $policyData = [
            'start_date' => $this->params['competition_date']->modify('midnight')->format('c'),
            'end_date' => $this->params['competition_date']->modify('tomorrow -1 sec')->format('c'),
            'sum_insured' => [
                'value' => $this->params['sum_insured'],
                'currency' => 'PLN',
            ],
            'addons' => [
                [
                    'code' => 'EVENT_NAME',
                    'value' => $this->params['competition_name'],
                ]
            ],
        ];

        if ($this->params['is_resignation_risk']) {
            $dt = new \DateTimeImmutable();
            $policyData['configured_risks'] = [
                [
                    'start_date' => $dt->modify('tomorrow')->format('c'),
                    'end_date' => $this->params['competition_date']->modify('tomorrow -1 sec')->format('c'),
                    'code' => 'KR',
                    'sum_insured' => [
                        'value' => $this->params['participation_fee'],
                        'currency' => 'PLN',
                    ],
                ],
            ];
        }

        return [
            'product_id' => $this->params['product_id'],
            'data' => $policyData,
            'policy_holder' => [
                'data' => $data,
                'address' => $address,
                'email' => $this->params['participant_email'],
                'telephone' => $this->params['participant_phone'],
                'agreements' => $agreements,
            ],
            'insureds' => [
                [
                    'data' => $data,
                    'address' => $address,
                    'options' => [
                        [
                            'code' => 'SPORT_TYPE',
                            'value' => $this->params['is_professional'] ? 'zawodowy' : 'amatorski',
                        ]
                    ]
                ]
            ],
        ];
    }
}
