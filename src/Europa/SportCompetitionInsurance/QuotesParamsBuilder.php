<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

/**
 * Class QuotesParamsBuilder
 *
 * @package Europa
 */
class QuotesParamsBuilder extends ParamsBuilder
{
    /**
     * @const array Required parameters, all others are optional.
     */
    const REQUIRED_PARAMS = [
        'sum_insured',
        'participation_fee',
        'competition_date',
        'is_professional',
        'is_resignation_risk',
        'participant_birth_date',
    ];

    /**
     * @inheritdoc
     */
    public function validate()
    {
        // Required parameters
        foreach (static::REQUIRED_PARAMS as $param) {
            if (!array_key_exists($param, $this->params)) {
                throw new \InvalidArgumentException('Required param "' . $param . '" is missing.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safelyBuild()
    {
        $policyData = [
            'start_date' => $this->params['competition_date']->modify('midnight')->format('c'),
            'end_date' => $this->params['competition_date']->modify('tomorrow -1 sec')->format('c'),
            'sum_insured' => [
                'value' => $this->params['sum_insured'],
                'currency' => 'PLN',
            ],
        ];

        if ($this->params['is_resignation_risk']) {
            $dt = new \DateTimeImmutable();
            $policyData['configured_risks'] = [
                [
                    'start_date' => $dt->modify('tomorrow')->format('c'),
                    'end_date' => $this->params['competition_date']->modify('tomorrow -1 sec')->format('c'),
                    'code' => 'KR',
                    'sum_insured' => [
                        'value' => $this->params['participation_fee'],
                        'currency' => 'PLN',
                    ],
                ],
            ];
        }

        return [
            'data' => $policyData,
            'prepersons' => [
                [
                    'birth_date' => $this->params['participant_birth_date']->format('Y-m-d'),
                    'options' => [
                        [
                            'code' => 'SPORT_TYPE',
                            'value' => $this->params['is_professional'] ? 'zawodowy' : 'amatorski',
                        ]
                    ]
                ]
            ],
        ];
    }
}
