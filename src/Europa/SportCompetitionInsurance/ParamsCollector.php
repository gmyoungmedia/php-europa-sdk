<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\SportCompetitionInsurance;

/**
 * Class ParamsCollector
 *
 * @package Europa
 */
class ParamsCollector
{
    /**
     * @var array The collected parameters.
     */
    protected $params;

    /**
     * Instantiates a new ParamsCollector collector.
     */
    public function __construct()
    {
        $this->params = [];
    }

    /**
     * Sets product id.
     *
     * @param string $productId The product id.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function productId($productId)
    {
        $this->validateString('product_id', $productId, 24);

        return $this->chain('product_id', $productId);
    }

    /**
     * Sets sum insured.
     *
     * @param float|int $sumInsured The insurance amount.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function sumInsured($sumInsured)
    {
        $this->validatePrice('sum_insured', $sumInsured); // not negative

        return $this->chain('sum_insured', floatval($sumInsured));
    }

    /**
     * Sets participation fee.
     *
     * @param float|int $participationFee The participation fee.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participationFee($participationFee)
    {
        $this->validatePrice('participation_fee', $participationFee); // not negative

        return $this->chain('participation_fee', floatval($participationFee));
    }

    /**
     * Sets competition name.
     *
     * @param string $competitionName The competition name.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function competitionName($competitionName)
    {
        $this->validateString('competition_name', $competitionName);

        return $this->chain('competition_name', $competitionName);
    }

    /**
     * Sets competition date.
     *
     * @param int|string $competitionDate The competition date as timestamp or date string.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function competitionDate($competitionDate)
    {
        $this->validateDate('competition_date', $competitionDate);

        return $this->chain('competition_date', $this->castToDateTime($competitionDate));
    }

    /**
     * Sets participant agreements.
     *
     * @param array $agreements The array with agreements codes.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function agreements(array $agreements)
    {
        $supportedAgreements = array_keys(CalculationParamsBuilder::AGREEMENTS_DATA);
        if (!empty(array_diff($agreements, $supportedAgreements))) {
            throw new \InvalidArgumentException('Param "agreements" must contain only supported agreements codes.');
        }
        if (count(array_count_values($agreements)) !== count($agreements)) {
            throw new \InvalidArgumentException('Param "agreements" must not contain duplicates.');
        }

        return $this->chain('agreements', $agreements);
    }

    /**
     * Sets if participant is professional.
     *
     * @param boolean $isProfessional True if participant is professional.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function isProfessional($isProfessional)
    {
        $this->validateBoolean('is_professional', $isProfessional);

        return $this->chain('is_professional', $isProfessional);
    }

    /**
     * Sets if insurance should include resignation risk.
     *
     * @param boolean $isResignationRisk True if resignation risk should be included.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function isResignationRisk($isResignationRisk)
    {
        $this->validateBoolean('is_resignation_risk', $isResignationRisk);

        return $this->chain('is_resignation_risk', $isResignationRisk);
    }

    /**
     * Sets participant first name.
     *
     * @param string $participantFirstName The participant first name.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantFirstName($participantFirstName)
    {
        $this->validateNotEmptyString('participant_first_name', $participantFirstName);

        return $this->chain('participant_first_name', $participantFirstName);
    }

    /**
     * Sets participant last name.
     *
     * @param string $participantLastName The participant last name.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantLastName($participantLastName)
    {
        $this->validateNotEmptyString('participant_last_name', $participantLastName);

        return $this->chain('participant_last_name', $participantLastName);
    }

    /**
     * Sets participant birth date.
     *
     * @param int|string $participantBirthDate The participant birthdate as timestamp or date string.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantBirthDate($participantBirthDate)
    {
        $this->validateDate('participant_birth_date', $participantBirthDate);

        return $this->chain('participant_birth_date', $this->castToDateTime($participantBirthDate));
    }

    /**
     * Sets participant pesel.
     *
     * @param string $participantPesel The valid polish pesel.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantPesel($participantPesel)
    {
        $this->validatePesel('participant_pesel', $participantPesel);

        return $this->chain('participant_pesel', $participantPesel);
    }

    /**
     * Sets participant email.
     *
     * @param string $participantEmail The valid email.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantEmail($participantEmail)
    {
        $this->validateEmail('participant_email', $participantEmail);

        return $this->chain('participant_email', $participantEmail);
    }

    /**
     * Sets participant phone number.
     *
     * @param string $participantPhone The valid phone number.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantPhone($participantPhone)
    {
        $this->validatePhone('participant_phone', $participantPhone);

        return $this->chain('participant_phone', $participantPhone);
    }

    /**
     * Sets participant country.
     *
     * @param string $participantCountry The two digit country code.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantCountry($participantCountry)
    {
        $this->validateCountry('participant_country', $participantCountry);

        return $this->chain('participant_country', $participantCountry);
    }

    /**
     * Sets participant city.
     *
     * @param string $participantCity The participant city.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantCity($participantCity)
    {
        $this->validateNotEmptyString('participant_city', $participantCity);

        return $this->chain('participant_city', $participantCity);
    }

    /**
     * Sets participant zipcode.
     *
     * @param string $participantZipcode The participant zipcode.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantZipcode($participantZipcode)
    {
        $this->validateNotEmptyString('participant_zipcode', $participantZipcode);

        return $this->chain('participant_zipcode', $participantZipcode);
    }

    /**
     * Sets participant street.
     *
     * @param string $participantStreet The participant street.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantStreet($participantStreet)
    {
        $this->validateNotEmptyString('participant_street', $participantStreet);

        return $this->chain('participant_street', $participantStreet);
    }

    /**
     * Sets participant house number.
     *
     * @param string $participantHouseNumber The participant house number.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantHouseNumber($participantHouseNumber)
    {
        $this->validateNotEmptyString('participant_house_number', $participantHouseNumber);

        return $this->chain('participant_house_number', $participantHouseNumber);
    }

    /**
     * Sets participant flat number.
     *
     * @param string $participantFlatNumber The participant flat number.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function participantFlatNumber($participantFlatNumber)
    {
        $this->validateNotEmptyString('participant_flat_number', $participantFlatNumber);

        return $this->chain('participant_flat_number', $participantFlatNumber);
    }

    /**
     * Sets calculation id.
     *
     * @param string $calculationId The calculation id.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function calculationId($calculationId)
    {
        $this->validateString('calculation_id', $calculationId, 24);

        return $this->chain('calculation_id', $calculationId);
    }

    /**
     * Sets calculation checksum.
     *
     * @param string $calculationChecksum The calculation checksum.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function calculationChecksum($calculationChecksum)
    {
        $this->validateString('calculation_checksum', $calculationChecksum, 32);

        return $this->chain('calculation_checksum', $calculationChecksum);
    }

    /**
     * Sets payment date.
     *
     * @param int|string $paymentDate The payment date as timestamp or date string.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function paymentDate($paymentDate)
    {
        $this->validateDate('payment_date', $paymentDate);

        return $this->chain('payment_date', $this->castToDateTime($paymentDate));
    }

    /**
     * Sets solicitor id (OWCA number).
     *
     * @param string $solicitorId The solictor id.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function solicitorId($solicitorId)
    {
        $this->validateString('solicitor_id', $solicitorId);

        return $this->chain('solicitor_id', $solicitorId);
    }

    /**
     * Sets agent id (insurance agent number).
     *
     * @param string $agentId The agent id.
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    public function agentId($agentId)
    {
        $this->validateString('agent_id', $agentId);

        return $this->chain('agent_id', $agentId);
    }

    /**
     * Determines if this is long enough string.
     *
     * @param string    $param
     * @param mixed     $value
     * @param int       $minLength
     *
     * @throws \InvalidArgumentException
     */
    protected function validateString($param, $value, $minLength = 0)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException('Param "' . $param .'" must be of type string.');
        }
        if (strlen($value) < $minLength) {
            throw new \InvalidArgumentException('Param "' . $param .'" must be at least ' . $minLength . ' characters length.');
        }
    }

    /**
     * Determines if string is not empty.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateNotEmptyString($param, $value)
    {
        $this->validateString($param, $value, 1);
    }

    /**
     * Determines if value is not negative.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validatePrice($param, $value)
    {
        $this->validateNumeric($param, $value);
        if (floatval($value) < 0) {
            throw new \InvalidArgumentException('Param "' . $param . '" must be not negative.');
        }
    }

    /**
     * Determines if value is a number or numeric string.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateNumeric($param, $value)
    {
        if (!is_numeric($value)) {
            throw new \InvalidArgumentException('Param "' . $param . '" must be a number or numeric string.');
        }
    }

    /**
     * Determines if value is of type boolean.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateBoolean($param, $value)
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException('Param "' . $param . '" must be of type boolean.');
        }
    }

    /**
     * Determines if this is a timestamp or date string.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateDate($param, $value)
    {
        // `DateTime` and `strtotime` understand the same date strings,
        // so we can use either of them to do the check.
        if (!(is_int($value) || (is_string($value) && strtotime($value) !== false)))  {
            throw new \InvalidArgumentException('Param "' . $param . '" must be a timestamp or date string.');
        }
    }

    /**
     * Validates against two digit country code.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateCountry($param, $value)
    {
        $this->validateString($param, $value);
        $detectCountryCode = '/^[A-Z]{2}$/';
        if (preg_match($detectCountryCode, $value) !== 1) {
            throw new \InvalidArgumentException('Param "' . $param . '" must match ' .$detectCountryCode . ' country code.');
        }
    }

    /**
     * Validates against polish pesel.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validatePesel($param, $value)
    {
        $this->validateString($param, $value);
        $detectPesel = '/^[0-9]{11}$/';
        if (preg_match($detectPesel, $value) !== 1) {
            throw new \InvalidArgumentException('Param "' . $param . '" must match ' .$detectPesel . ' pesel.');
        }
        $ctrlSum = (9*$value[0] + 7*$value[1] + 3*$value[2] + 1*$value[3] + 9*$value[4] + 7*$value[5] + 3*$value[6] + 1*$value[7] + 9*$value[8] + 7*$value[9]) % 10;
        $correctCtrlSum = (int) $value[10];
        if ($ctrlSum !== $correctCtrlSum) {
            throw new \InvalidArgumentException('Param "' . $param . '" must contain valid pesel control sum.');
        }
    }

    /**
     * Validates against email address.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validateEmail($param, $value)
    {
        $this->validateString($param, $value);
        if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            throw new \InvalidArgumentException('Param "' . $param . '" must be an email address.');
        }
    }

    /**
     * Validates against phone number.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @throws \InvalidArgumentException
     */
    protected function validatePhone($param, $value)
    {
        $this->validateString($param, $value);
        $detectPhone = '/^\+((48[0-9]{9})|((?!48)[0-9]{6,15}))$/';
        if (preg_match($detectPhone, $value) !== 1) {
            throw new \InvalidArgumentException('Param "' . $param . '" must match ' .$detectPhone . ' phone.');
        }
    }

    /**
     * Casts a date value to DateTime.
     *
     * @param int|string $value
     *
     * @return \DateTime
     */
    protected function castToDateTime($value)
    {
        if (is_int($value)) {
            $dt = new \DateTime();
            $dt->setTimestamp($value);
        } else {
            $dt = new \DateTime($value);
        }

        return $dt;
    }

    /**
     * Sets unique parameter.
     *
     * @param string    $param
     * @param mixed     $value
     *
     * @return ParamsCollector
     *
     * @throws \InvalidArgumentException
     */
    protected function chain($param, $value)
    {
        if (isset($this->params['name'])) {
            throw new \InvalidArgumentException('Param "' . $param .'" already set.');
        }
        $this->params[$param] = $value;

        return $this;
    }

    /**
     * Returns collected params.
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}
