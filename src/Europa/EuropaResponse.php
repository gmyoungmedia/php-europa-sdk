<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa;

use Europa\ApiObjects\ApiObjectFactory;
use Europa\Exceptions\EuropaResponseException;

/**
 * Class EuropaResponse
 *
 * @package Europa
 */
class EuropaResponse
{
    /**
     * @var EuropaRequest The original request that returned this response.
     */
    protected $request;

    /**
     * @var array The headers returned from API.
     */
    protected $headers;

    /**
     * @var string The raw body of the response from API.
     */
    protected $rawBody;

    /**
     * @var array The decoded body of the response from API.
     */
    protected $body;

    /**
     * @var int The HTTP status code response from API.
     */
    protected $httpStatusCode;

    /**
     * @var EuropaResponseException The exception thrown by this request.
     */
    protected $thrownException;

    /**
     * Instantiates a new Response entity.
     *
     * @param EuropaRequest     $request
     * @param array             $headers
     * @param string            $rawBody
     * @param int               $httpStatusCode
     */
    public function __construct(EuropaRequest $request, array $headers, $rawBody, $httpStatusCode)
    {
        $this->request = $request;
        $this->headers = $headers;
        $this->rawBody = $rawBody;
        $this->httpStatusCode = $httpStatusCode;

        $this->decodeBodyAndError();
    }

    /**
     * Decodes the raw JSON body into an array and thrown exception.
     */
    public function decodeBodyAndError()
    {
        if ($this->request->isEchoRequest()) {
            $this->body = $this->rawBody;
        } else {
            $this->body = json_decode($this->rawBody, true);
        }

        if ($this->isError()) {
            $this->createException();
        }
    }

    /**
     * Returns true if API returned an error message.
     *
     * @return boolean
     */
    public function isError()
    {
        $successHttpStatusCodes = [200, 201];

        return !in_array($this->httpStatusCode, $successHttpStatusCodes);
    }

    /**
     * Creates an exception to be thrown later.
     */
    public function createException()
    {
        $this->thrownException = EuropaResponseException::create($this);
    }

    /**
     * Return the original request that returned this response.
     *
     * @return EuropaRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Return the HTTP headers for this response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Returns the raw body response.
     *
     * @return string
     */
    public function getRawBody()
    {
        return $this->rawBody;
    }

    /**
     * Returns the decoded body response.
     *
     * @return array
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return the HTTP status code for this response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Returns the exception that was thrown for this request.
     *
     * @return EuropaResponseException|null
     */
    public function getThrownException()
    {
        return $this->thrownException;
    }

    /**
     * Convenience method for creating an ApiList of ApiQuote's.
     *
     * @return \Europa\ApiObjects\ApiList
     *
     * @throws EuropaSDKException
     */
    public function getQuotes()
    {
        $factory = new ApiObjectFactory($this);

        return $factory->createApiQuoteList();
    }

    /**
     * Convenience method for creating an ApiCalculation.
     *
     * @return \Europa\ApiObjects\ApiCalculation
     *
     * @throws EuropaSDKException
     */
    public function getCalculation()
    {
        $factory = new ApiObjectFactory($this);

        return $factory->createApiCalculation();
    }

    /**
     * Convenience method for creating an ApiPolicy.
     *
     * @return \Europa\ApiObjects\ApiPolicy
     *
     * @throws EuropaSDKException
     */
    public function getPolicy()
    {
        $factory = new ApiObjectFactory($this);

        return $factory->createApiPolicy();
    }
}
