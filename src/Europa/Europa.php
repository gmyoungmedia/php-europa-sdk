<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa;

use Europa\HttpClients\EuropaCurlHttpClient;
use Europa\Exceptions\EuropaSDKException;
use Europa\SportCompetitionInsurance\SportCompetitionInsuranceHelper;

/**
 * Main class
 *
 * @package Europa
 */
class Europa
{
    /**
     * @const string Version number of the Europa PHP SDK.
     */
    const VERSION = '1.0.0';

    /**
     * @const string Default API version for requests.
     */
    const DEFAULT_API_VERSION = 'v2';

    /**
     * @var EuropaOperator The EuropaOperator entity.
     */
    protected $operator;

    /**
     * @var EuropaClient The Europa client service.
     */
    protected $client;

    /**
     * @var string The API version we want to use.
     */
    protected $apiVersion;

    /**
     * Instantiates a new Europa super-class object.
     *
     * @param array $config
     *
     * @throws EuropaSDKException
     */
    public function __construct(array $config = [])
    {
        $config = array_merge([
            'customer_id' => null,
            'api_version' => static::DEFAULT_API_VERSION,
        ], $config);

        if (!$config['customer_id']) {
            throw new EuropaSDKException('Required "customer_id" key not supplied in config.');
        }

        $this->operator = new EuropaOperator($config['customer_id']);
        $this->client = new EuropaClient(new EuropaCurlHttpClient());
        $this->apiVersion = $config['api_version'];
    }

    /**
     * Tests API connection.
     *
     * @return boolean
     */
    public function echoApi()
    {
        $request = $this->request('GET', '/echo');

        $response = $this->client->sendRequest($request);

        return $response->getBody() === 'echo';
    }

    /**
     * Instantiates a new EuropaRequest entity.
     *
     * @param string    $method
     * @param string    $endpoint
     * @param array     $params
     *
     * @return EuropaRequest
     */
    public function request($method, $endpoint, array $params = [])
    {
        return new EuropaRequest(
            $this->operator,
            $method,
            $endpoint,
            $params,
            $this->apiVersion
        );
    }

    /**
     * Returns the sport competition insurance helper.
     *
     * @return SportCompetitionInsuranceHelper
     */
    public function getSportCompetitionInsuranceHelper()
    {
        return new SportCompetitionInsuranceHelper($this->operator, $this->client, $this->apiVersion);
    }
}
