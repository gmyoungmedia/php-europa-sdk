<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa;

use Europa\HttpClients\EuropaHttpClient;
use Europa\Exceptions\EuropaSDKException;

/**
 * Class EuropaClient
 *
 * @package Europa
 */
class EuropaClient
{
   /**
     * @const string Production API URL.
     */
    const BASE_URL = 'https://api.tueuropa.pl';

    /**
     * @const int The timeout in seconds for a normal request.
     */
    const DEFAULT_REQUEST_TIMEOUT = 10;

    /**
     * @var EuropaHttpClient HTTP client handler.
     */
    protected $httpClient;

    /**
     * @var int The number of calls that have been made to API.
     */
    public static $requestCount = 0;

    /**
     * Instantiates a new EuropaClient object.
     *
     * @param EuropaHttpClient $httpClient
     */
    public function __construct(EuropaHttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Makes the request to API and returns the response.
     *
     * @param EuropaRequest $request
     *
     * @return EuropaResponse
     *
     * @throws EuropaSDKException
     */
    public function sendRequest(EuropaRequest $request)
    {
        list($url, $method, $headers, $body) = $this->prepareRequestMessage($request);

        $apiResponse = $this->httpClient->send($url, $method, $headers, $body, static::DEFAULT_REQUEST_TIMEOUT);

        static::$requestCount++;

        $response = new EuropaResponse(
            $request,
            $apiResponse->getHeaders(),
            $apiResponse->getBody(),
            $apiResponse->getHttpStatusCode()
        );

        if ($response->isError()) {
            throw $response->getThrownException();
        }

        return $response;
    }

    /**
     * Prepares the request for sending to the client handler.
     *
     * @param EuropaRequest $request
     *
     * @return array
     */
    public function prepareRequestMessage(EuropaRequest $request)
    {
        $url = static::BASE_URL . $request->getRelativeUrl();

        return [
            $url,
            $request->getMethod(),
            $request->getHeaders(),
            $request->getJsonEncodedBody(),
        ];
    }
}
