<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\Http;

/**
 * Class ApiCurlResponse
 *
 * @package Europa
 */
class ApiCurlResponse extends ApiResponse
{
    /**
     * Creates a new ApiCurlResponse entity.
     *
     * @param array $rawResponse The raw HTTP response.
     */
    function __construct($rawResponse)
    {
        parent::__construct($rawResponse);
    }

    /**
     * @inheritdoc
     */
    function parseRawResponse($rawResponse)
    {
        list($rawHeaders, $rawBody) = $this->separateResponseHeadersAndBody($rawResponse);
        list($headers, $httpStatusCode) = $this->parseHeaders($rawHeaders);

        return [$headers, $rawBody, $httpStatusCode];
    }


    /**
     * Separate the headers and the body into a two-part array
     *
     * @param array $rawResponse The raw HTTP response.
     *
     * @return array
     */
    public function separateResponseHeadersAndBody($rawResponse)
    {
        $parts = explode("\r\n\r\n", $rawResponse);
        $rawBody = array_pop($parts);
        $rawHeaders = implode("\r\n\r\n", $parts);

        return [trim($rawHeaders), trim($rawBody)];
    }

    /**
     * Parse the raw headers and return as an array.
     *
     * @param string $rawHeaders The raw headers from the response.
     *
     * @return array
     */
    protected function parseHeaders($rawHeaders)
    {
        // Normalize line breaks
        $rawHeaders = str_replace("\r\n", "\n", $rawHeaders);

        // There will be multiple headers if a 301 was followed
        // or a proxy was followed, etc
        $headerCollection = explode("\n\n", trim($rawHeaders));

        // We just want the last response (at the end)
        $rawHeaders = array_pop($headerCollection);

        // Explode into an array
        $rawHeaders = explode("\n", $rawHeaders);

        return $this->decompileRequestHeaders($rawHeaders);
    }

    /**
     * Decompiles the response headers from a curl-friendly format.
     *
     * @param array $rawHeaders The response headers.
     *
     * @return array
     */
    public function decompileRequestHeaders(array $rawHeaders)
    {
        $headers = [];
        $httpStatusCode = null;

        foreach ($rawHeaders as $rawHeader) {
            if (strpos($rawHeader, ': ') === false) {
                $httpStatusCode = $this->extractHttpStatusCodeFromHeader($rawHeader);
            } else {
                list($key, $value) = explode(': ', $rawHeader, 2);
                $headers[$key] = $value;
            }
        }

        return [$headers, $httpStatusCode];
    }

    /**
     * Sets the HTTP response code from a raw header.
     *
     * @param string $rawHeader
     *
     * @return int
     */
    public function extractHttpStatusCodeFromHeader($rawHeader)
    {
        preg_match('|HTTP/\d\.\d\s+(\d+)\s+.*|', $rawHeader, $match);

        return (int) $match[1];
    }
}
