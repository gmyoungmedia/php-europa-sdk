<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\Http;

/**
 * Abstract Class ApiResponse
 *
 * @package Europa
 */
abstract class ApiResponse
{
    /**
     * @var array The response headers in the form of an associative array.
     */
    protected $headers;

    /**
     * @var string The raw response body.
     */
    protected $body;

    /**
     * @var int The HTTP status response code.
     */
    protected $httpStatusCode;

    /**
     * Creates a new ApiResponse entity.
     *
     * @param array $rawResponse The raw HTTP response.
     */
    function __construct($rawResponse)
    {
        list($this->headers, $this->body, $this->httpStatusCode) = $this->parseRawResponse($rawResponse);
    }

    /**
     * Parses raw HTTP response into associative array.
     *
     * @param array $rawResponse The raw HTTP response.
     *
     * @return array
     */
    abstract function parseRawResponse($rawResponse);

    /**
     * Return the response headers.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Return the body of the response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return the HTTP status code for this response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }
}
