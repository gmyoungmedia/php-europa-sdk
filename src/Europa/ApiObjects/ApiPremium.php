<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiPremium
 *
 * @package Europa
 */
class ApiPremium extends ApiObject
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [
        'payment_schema' => '\Europa\ApiObjects\ApiPaymentSchema',
        'date_rate' => '\Europa\ApiObjects\ApiDate',
    ];

    /**
     * Returns the `value` (Insurance premium value) as double.
     *
     * @return double
     */
    public function getValue()
    {
        return $this->getField('value');
    }

    /**
     * Returns the `value_currency` (Insurance premium value currency) as ISO 4217 string.
     *
     * @return string
     */
    public function getValueCurrency()
    {
        return $this->getField('value_currency');
    }
}
