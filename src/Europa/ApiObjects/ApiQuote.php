<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiQuote
 *
 * @package Europa
 */
class ApiQuote extends ApiObject
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [
        'payment_schema' => '\Europa\ApiObjects\ApiPaymentSchema',
        'premium' => '\Europa\ApiObjects\ApiPremium',
        'tariff_premium' => '\Europa\ApiObjects\ApiPremium',
        'risks_definitions' => '\Europa\ApiObjects\ApiRisk',
        'details' => '\Europa\ApiObjects\ApiDetail',
    ];

    /**
     * Returns the `product_id` (Insurance product id) as string.
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getField('product_id');
    }

    /**
     * Returns the `premium` (Insurance premium) as ApiPremium.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the `name` (Insurance name) as string.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns true if quote has resignation risk defined.
     *
     * @return boolean
     */
    public function hasResignationRisk()
    {
        $risksDefinitions = $this->getField('risks_definitions', array());

        foreach ($risksDefinitions as $riskDefinition) {
            if ($riskDefinition->isResignationRisk()) {

                return true;
            }
        }

        return false;
    }
}
