<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiObject
 *
 * @package Europa
 */
class ApiObject extends Collection
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [];

    /**
     * Instantiate a new ApiObject.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct($this->castFields($data));
    }

    /**
     * Iterates over an array and detects the types each object
     * should be cast to and returns all the fields as an array.
     *
     * @param array $data The array to iterate over.
     *
     * @todo Cast primitive fields if relevant
     *
     * @return array
     */
    public function castFields(array $data)
    {
        $fields = [];

        foreach ($data as $k => $v) {
            // if ($k === 'date') {
            //     $fields[$k] = $this->castToDateTime($v);
            // } else {
            //     $fields[$k] = $v;
            // }
            $fields[$k] = $v;
        }

        return $fields;
    }

    /**
     * Casts a date value from Api to DateTime.
     *
     * @param string $value
     *
     * @return \DateTime
     */
    public function castToDateTime($value)
    {
        return new \DateTime($value);
    }

    /**
     * Gets the value of a field in the object.
     *
     * @param string $name    The field to retrieve.
     * @param mixed  $default The default to return if the field doesn't exist.
     *
     * @return mixed
     */
    public function getField($name, $default = null)
    {
        if (isset($this->items[$name])) {
            return $this->items[$name];
        }

        return $default;
    }

    /**
     * Getter for $objectMap.
     *
     * @return array
     */
    public static function getObjectMap()
    {
        return static::$objectMap;
    }
}
