<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiCalculation
 *
 * @package Europa
 */
class ApiCalculation extends ApiObject
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [
        'premium' => '\Europa\ApiObjects\ApiPremium',
        'tariff_premium' => '\Europa\ApiObjects\ApiPremium',
    ];

    /**
     * Returns the `calculation_id` (Calculation id) as string.
     *
     * @return string
     */
    public function getCalculationId()
    {
        return $this->getField('calculation_id');
    }

    /**
     * Returns the `premium` (Calculation premium) as ApiPremium.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the `checksum` (Calculation checksum) as string.
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->getField('checksum');
    }
}
