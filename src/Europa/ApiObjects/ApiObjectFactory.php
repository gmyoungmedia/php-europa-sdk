<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

use Europa\EuropaResponse;
use Europa\Exceptions\EuropaSDKException;

/**
 * Class ApiObjectFactory
 *
 * @package Europa
 *
 * ## Assumptions ##
 * ApiList - is ALWAYS a sequential numeric array
 * ApiList - is ALWAYS an array of ApiObject types
 * ApiObject - is ALWAYS an associative array
 * ApiObject - MAY contain ApiObject's "recurrable"
 * ApiObject - MAY contain ApiList's "recurrable"
 * ApiObject - MAY contain DateTime's "primitives"
 * ApiObject - MAY contain string's "primitives"
 * ApiObject - MAY contain float's "primitives"
 * ApiObject - MAY contain boolean's "primitives"
 */
class ApiObjectFactory
{
    /**
     * @const string The base Api object class.
     */
    const BASE_API_OBJECT_CLASS = '\Europa\ApiObjects\ApiObject';

    /**
     * @const string The base Api list class.
     */
    const BASE_API_LIST_CLASS = '\Europa\ApiObjects\ApiList';

    /**
     * @const string The base Api object prefix.
     */
    const BASE_API_OBJECT_PREFIX = '\Europa\ApiObjects\\';

    /**
     * @var array The body of the EuropaResponse entity from Api.
     */
    protected $body;

    /**
     * Instatiate a new ApiObjectFactory factory.
     *
     * @param EuropaResponse $response The response entity from Api.
     */
    public function __construct(EuropaResponse $response)
    {
        $this->body = $response->getBody();

        $this->validateResponseAsArray();
    }

    /**
     * Validates that the response data is an array.
     *
     * @throws EuropaSDKException
     */
    public function validateResponseAsArray()
    {
        if (!is_array($this->body)) {
            throw new EuropaSDKException('Unable to get response from Api as array.', 620);
        }
    }

    /**
     * Convenience method for creating an ApiList of ApiQuote's.
     *
     * @return ApiList
     *
     * @throws EuropaSDKException
     */
    public function createApiQuoteList()
    {
        return $this->createApiList($this->body, static::BASE_API_OBJECT_PREFIX . 'ApiQuote');
    }

    /**
     * Convenience method for creating an ApiCalculation.
     *
     * @return ApiCalculation
     *
     * @throws EuropaSDKException
     */
    public function createApiCalculation()
    {
        return $this->createApiObject($this->body, static::BASE_API_OBJECT_PREFIX . 'ApiCalculation');
    }

    /**
     * Convenience method for creating an ApiPolicy.
     *
     * @return ApiPolicy
     *
     * @throws EuropaSDKException
     */
    public function createApiPolicy()
    {
        return $this->createApiObject($this->body, static::BASE_API_OBJECT_PREFIX . 'ApiPolicy');
    }

    /**
     * Takes an array of data and determines how to cast it.
     *
     * @param array     $data       The array of data to be casted.
     * @param string    $subclass   The subclass to cast the data to.
     *
     * @return ApiObject|ApiList
     *
     * @throws EuropaSDKException
     */
    public function createApiObjectOrApiList(array $data, $subclass)
    {
        if (static::isCastableAsApiList($data)) {
            // Create ApiList
            return $this->createApiList($data, $subclass);
        }

        // Create ApiObject
        return $this->createApiObject($data, $subclass);
    }

    /**
     * Determines whether or not the data should be cast as a ApiList.
     *
     * @param array $data
     *
     * @return boolean
     */
    public static function isCastableAsApiList(array $data)
    {
        if ($data === []) {
            return true;
        }

        // Checks for a sequential numeric array which would be an ApiList
        return array_keys($data) === range(0, count($data) - 1);
    }

    /**
     * Instantiates a new $subclass of ApiObject.
     *
     * @param array     $data       The array of object data.
     * @param string    $subclass   The subclass to cast this object to.
     *
     * @return ApiObject
     *
     * @throws EuropaSDKException
     */
    public function createApiObject(array $data, $subclass)
    {
        static::validateSubclass($subclass);

        $fields = [];

        foreach ($data as $k => $v) {
            if (is_array($v)) {
                // Detect any smart-casting from the $objectMap array.
                // This is always empty on the ApiObject collection, but subclasses can define
                // their own array of smart-casting types.
                $objectMap = $subclass::getObjectMap();
                $vSublass = isset($objectMap[$k]) ? $objectMap[$k] : null;

                // An ApiObject or ApiList
                $fields[$k] = $this->createApiObjectOrApiList($v, $vSublass);
            } else {
                // A primitive
                $fields[$k] = $v;
            }
        }

        return new $subclass($fields);
    }

    /**
     * Instatiates a new ApiList of ApiObject's.
     *
     * @param array     $data       The array of objects.
     * @param string    $subclass   The subclass to cast each object in the list to.
     *
     * @return ApiList
     *
     * @throws EuropaSDKException
     */
    public function createApiList(array $data, $subclass)
    {
        $items = [];

        foreach ($data as $v) {
            $items[] = $this->createApiObject($v, $subclass);
        }

        $class = static::BASE_API_LIST_CLASS;

        return new $class($items);
    }

    /**
     * Ensures that the subclass in question is indeed a sublass of ApiObject.
     *
     * @param string $subclass The ApiObject subclass to validate.
     *
     * @throws EuropaSDKException
     */
    public static function validateSubclass($subclass)
    {
        if ($subclass == static::BASE_API_OBJECT_CLASS || is_subclass_of($subclass, static::BASE_API_OBJECT_CLASS)) {
            return;
        }

        throw new EuropaSDKException('The given subclass "' . $subclass . '" is not a sublass of ApiObject class.', 620);
    }
}
