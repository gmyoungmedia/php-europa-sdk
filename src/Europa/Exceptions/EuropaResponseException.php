<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\Exceptions;

use Europa\EuropaResponse;

/**
 * Class EuropaResponseException
 *
 * @package Europa
 */
class EuropaResponseException extends EuropaSDKException
{
    /**
     * @var EuropaResponse The response that threw the exception.
     */
    protected $response;

    /**
     * Creates a new EuropaResponseException entity.
     *
     * @param EuropaResponse     $response          The response that threw the exception.
     * @param EuropaSDKException $previousException The more detailed exception.
     */
    public function __construct(EuropaResponse $response, EuropaSDKException $previousException)
    {
        parent::__construct($previousException->getMessage(), $previousException->getCode(), $previousException);

        $this->response = $response;
    }

    /**
     * A factory for creating the appropriate exception based on the response from Graph.
     *
     * @param EuropaResponse $response The response that threw the exception.
     *
     * @return EuropaResponseException
     */
    public static function create(EuropaResponse $response)
    {
        $code = $response->getHttpStatusCode();
        $message = $response->getRawBody();

        switch ($code) {
            // Bad request
            case 404:
            case 406:
                return new static($response, new EuropaRequestException($message, $code));

            // Validation error
            case 400:
            case 422:
            case 423:
                return new static($response, new EuropaValidationException($message, $code));

            // Authentication error
            case 401:
                return new static($response, new EuropaAuthenticationException($message, $code));

            // Missing permissions
            case 403:
                return new static($response, new EuropaAuthorizationException($message, $code));

            // Server issue
            case 500:
                return new static($response, new EuropaServerException($message, $code));

            // All others
            default:
                return new static($response, new EuropaOtherException($message, $code));
        }
    }
}
