<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa;

/**
 * Class EuropaRequest
 *
 * @package Europa
 */
class EuropaRequest
{
    /**
     * @var EuropaOperator The EuropaOperator entity.
     */
    protected $operator;

    /**
     * @var string The HTTP method for this request.
     */
    protected $method;

    /**
     * @var string The API endpoint for this request.
     */
    protected $endpoint;

    /**
     * @var array The parameters to send with this request.
     */
    public $params;

    /**
     * @var string API version to use for this request.
     */
    protected $apiVersion;

    /**
     * Instantiates a new Request entity.
     *
     * @param EuropaOperator    $operator
     * @param string            $method
     * @param string            $endpoint
     * @param array             $params
     * @param string            $apiVersion
     */
    public function __construct(EuropaOperator $operator, $method, $endpoint, array $params = [], $apiVersion = Europa::DEFAULT_API_VERSION)
    {
        $this->operator = $operator;
        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->params = $params;
        $this->apiVersion = $apiVersion;
    }

    /**
     * Generate and return the relative URL for this request.
     *
     * @return string
     */
    public function getRelativeUrl()
    {
        $url = ($this->isEchoRequest() ? '' : '/insurance/' . $this->apiVersion) . $this->endpoint . '?';

        if (!$this->isEchoRequest()) {
            $url .= http_build_query(['customer_id' => $this->operator->getCustomerId()], null, '&');
        }

        if ($this->method !== 'POST') {
            $url .= http_build_query($this->params, null, '&');
        }

        return $url;
    }

    /**
     * Return the HTTP method for this request.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Generate and return the headers for this request.
     *
     * @return array
     */
    public function getHeaders()
    {
        return [
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * Returns the body of the request as URL-encoded.
     *
     * @return string
     */
    public function getJsonEncodedBody()
    {
        $postParams = $this->method === 'POST' ? $this->params : [];

        return json_encode($postParams);
    }

    /**
     * Returns true if this is echo request.
     *
     * @return string
     */
    public function isEchoRequest()
    {
        return $this->endpoint == '/echo';
    }
}
