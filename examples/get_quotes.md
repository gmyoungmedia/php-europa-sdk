# Get preliminary quotes
---
This example shows how to get quotes using Europa API and SDK for PHP.

A quote is a kind of simplified insurance offer. To get a quote only basic information is required, such as participant birth date and participation fee. The returned quote contains some insurance data like name and price.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}'
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

try {
    // Set params
    $helper->paramsToSet()
        ->sumInsured('30000')
        ->participationFee('60.99')
        ->competitionDate('2020-12-31T08:00:00+02:00')
        ->participantBirthDate('1950-12-31');

    // Get quotes
    $quotes = $helper->getQuotes();
} catch (\InvalidArgumentException $e) {
    // When params validation fails
    echo 'Params validation failed: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error
    echo 'API returned an error: ' . json_decode($e->getMessage());
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues
    echo 'Europa SDK returned an error: ' . json_decode($e->getMessage());
    exit;
}

// Print details
var_dump($quotes);
```
