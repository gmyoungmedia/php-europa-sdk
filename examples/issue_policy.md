# Issue insurance policy
---
This example shows how to issue insurance policy using Europa API and SDK for PHP.

To issue insurance policy you need to pass previoulsy made calculation id and checksum. There is no need to send the returned policy to the participant, as the Europa API does this automatically.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}'
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

try {
    // Set params
    $helper->paramsToSet()
        ->calculationId('{calculation-id}')
        ->calculationChecksum('{calculation-checksum}')
        ->paymentDate('2017-12-31T08:00:00+02:00')
        ->solicitorId('{solicitor-id}')
        ->agentId('{agent-id}');

    // Issue policy
    $policy = $helper->issuePolicy();
} catch (\InvalidArgumentException $e) {
    // When params validation fails
    echo 'Params validation failed: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error
    echo 'API returned an error: ' . json_decode($e->getMessage());
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues
    echo 'Europa SDK returned an error: ' . json_decode($e->getMessage());
    exit;
}

// Print details
var_dump($policy);
```
