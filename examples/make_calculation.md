# Make final insurance calculation
---
This example shows how to make calculation using Europa API and SDK for PHP.

Calculation is a final insurance offer. To make a calculation a very precise information is required. The returned calculation contains final insurance price and other insurance details.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}'
]);

$helper = $europa->getSportCompetitionInsuranceHelper();

try {
    // Set params
    $helper->paramsToSet()
        ->productId('{product-id}')
        ->sumInsured('30000')
        ->participationFee('60.99')
        ->competitionName('Foo competition')
        ->competitionDate('2020-12-31T08:00:00+02:00')
        ->agreements(['ZGOBJ', 'ZGPOTW', 'ZGREGODS'])
        ->isProfessional(false)
        ->isResignationRisk(true)
        ->participantFirstName('John')
        ->participantLastName('Doe')
        ->participantBirthDate('1950-12-31')
        ->participantPesel('50123100011')
        ->participantEmail('mail@example.com')
        ->participantPhone('+48123123123')
        ->participantCountry('PL')
        ->participantCity('City')
        ->participantZipcode('00-001')
        ->participantStreet('Street')
        ->participantHouseNumber('1/2')
        ->participantFlatNumber('3A');

    // Make calculation
    $calculation = $helper->makeCalculation();
} catch (\InvalidArgumentException $e) {
    // When params validation fails
    echo 'Params validation failed: ' . $e->getMessage();
    exit;
} catch (Europa\Exceptions\EuropaResponseException $e) {
    // When API returns an error
    echo 'API returned an error: ' . json_decode($e->getMessage());
    exit;
} catch (Europa\Exceptions\EuropaSDKException $e) {
    // Other issues
    echo 'Europa SDK returned an error: ' . json_decode($e->getMessage());
    exit;
}

// Print details
var_dump($calculation);
```
