# Echo API
---
This example shows how to test connection with Europa API using SDK for PHP.

## Example
---

```php
<?php
$europa = new Europa\Europa([
    'customer_id' => '{customer-id}'
]);

// Echo returns `true` on success
var_dump($europa->echoApi());
```
