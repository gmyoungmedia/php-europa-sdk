<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiPolicy
 *
 * @package Europa
 */
class ApiPolicy extends ApiObject
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [
        'policy_date' => '\Europa\ApiObjects\ApiDate',
        'premium' => '\Europa\ApiObjects\ApiPremium',
        'tariff_premium' => '\Europa\ApiObjects\ApiPremium',
        'policy_holder' => '\Europa\ApiObjects\ApiPolicyHolder',
        'insureds' => '\Europa\ApiObjects\ApiInsured',
    ];

    /**
     * Returns the `policy_id` (Policy id) as string.
     *
     * @return string
     */
    public function getPolicyId()
    {
        return $this->getField('policy_id');
    }

    /**
     * Returns the `policy_number` (Policy number) as string.
     *
     * @return string
     */
    public function getPolicyNumber()
    {
        return $this->getField('policy_number');
    }

    /**
     * Returns the `policy_date` (Policy date) as ApiDate.
     *
     * @return ApiDate
     */
    public function getPolicyDate()
    {
        return new $this->getField('policy_date');
    }

    /**
     * Returns the `premium` (Policy premium) as ApiPremium.
     *
     * @return ApiPremium
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the `checksum` (Policy checksum) as string.
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->getField('checksum');
    }
}

