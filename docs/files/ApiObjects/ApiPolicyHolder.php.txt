<?php
/**
 * Copyright 2018 Youngmedia.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
namespace Europa\ApiObjects;

/**
 * Class ApiPolicyHolder
 *
 * @package Europa
 */
class ApiPolicyHolder extends ApiObject
{
    /**
     * @var array Maps collection keys to api object types.
     */
    protected static $objectMap = [
        'data' => '\Europa\ApiObjects\ApiPerson',
        'address' => '\Europa\ApiObjects\ApiAddress',
        'agreements' => '\Europa\ApiObjects\ApiAgreement',
    ];
}

